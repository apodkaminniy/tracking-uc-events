Repository for tracking uc panel events.

To track events run, node index.js

Uc events are available by the route /uc_events/{org_id}

Blf events are available by the route /uc_blf_events/{org_id}